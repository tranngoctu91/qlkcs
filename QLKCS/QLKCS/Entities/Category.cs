﻿using QLKCS.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKCS.Entities
{
    [Table("Categories")]
    public class Category
    {
        [Key]
        public Guid CategoryId { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        [Required]
        public string CategoryName { get; set; }

        [Column(TypeName = "datetime")]
        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public EntityStatus Status { get; set; }

        public IList<Product> Products { get; set; }
    }
}

